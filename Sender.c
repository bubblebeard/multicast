#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

#define PORT 8888
#define GROUP "225.0.0.37"

int main(int argc, char** argv) {
    struct sockaddr_in addr;
    int fd;
    ssize_t data;
    char *message = "Hello, server!";
    if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket");
        exit(1);
    }

    memset(&addr, 0, sizeof (addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(GROUP);
    addr.sin_port = htons(PORT);

    for (;;) {

        if ((data = sendto(fd, message, strlen(message), 0, (struct sockaddr*) &addr, sizeof (addr))) < 0) {
            perror("sendto");
            exit(1);
        }
        printf("%d\n", data);
        sleep(1);
    }

    return (EXIT_SUCCESS);
}

