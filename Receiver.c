#include <stdio.h>
#include <linux/in.h>
#include <sys/types.h>
#include <sys/socket.h>
//#include <netinet/in.h>
//#include <arpa/inet.h>
#include <time.h>
#include <string.h>
//gcc -E test.c -o test

#define PORT 8888
#define GROUP "225.0.0.37"
#define MSGSIZE 256


int main(int argc, char* argv[]) {
    struct sockaddr_in addr;
    int fd, bytes, addrlen;
    struct ip_mreq req;
    char buf[MSGSIZE];
    int yes;

    if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket");
        exit(1);
    }

    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof (yes)) < 0) {
        perror("setsockopt");
        exit(1);
    }

    memset(&addr, 0, sizeof (addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(PORT);

    if (bind(fd, (struct sockaddr *) &addr, sizeof (addr)) < 0) {
        perror("bind");
        exit(1);
    }
    
    req.imr_multiaddr.s_addr = inet_addr(GROUP);
    req.imr_interface.s_addr = htonl(INADDR_ANY);

    if (setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &req, sizeof (req)) < 0) {
        perror("setsockopt");
        exit(1);
    }

    addrlen = sizeof (addr);

    for (;;) {
        if (recvfrom(fd, buf, MSGSIZE, 0, (struct sockaddr *) &addr, &addrlen) < 0) {
            perror("recvfrom");
            exit(1);
        }
        puts(buf);
    }
}